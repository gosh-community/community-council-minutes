Pierre and Liz information exchange notes from chatting


References that Pierre has experience with:

Wellcome Trust - Open Research 2019: https://wellcome.org/what-we-do/our-work/open-research

Journal of Open Hardware: https://openhardware.metajnl.com/

NIH Grand Challenge: https://grants.nih.gov/grants/funding/funding_program.htm

Pierre and Liz both mentored the "Open Hardware Makers" program's development.  

reGOSH: https://regosh.libres.cc/


Operating principles we'd like to see in our funding distribution

- transparency 

- eligibility criteria

- "concept notes" that are submitted will be posted publicly (is this an issue for the vulnerable?)

- we must see how we can protect these applicants

- Policy on intellectual property

- have two rounds: a 6 month and a 9-12 month. this will allow us to get results from the first round and see how we can iteratively improve the way to distribute funding to reach GOSH goals. 


****
**EXISTING TIMELINE** The deadline for funds to be spent is April 30, 2022, so we are making a case for an extension. 
****

PROPOSED TIMELINE. Note: phrases below "in quotes" are pulled directly from the grant document here: https://docs.google.com/document/d/1yxhi8FzfH7Wlk80R3AsOJK1xGDjRq1GKsFEf3x-JKHM/edit#heading=h.izc8s1qux688

2021 

September: 

Choose council member to chair the Funding Distribution Working Group: Pierre

Add council person to support the creation of the timeline: Liz

Review documents: https://docs.google.com/document/d/1yxhi8FzfH7Wlk80R3AsOJK1xGDjRq1GKsFEf3x-JKHM/edit#heading=h.izc8s1qux688 that shows three ~month-long workshops running per year for two years with external professionals bringing in expertise that is not already in GOSH. Read in the grant that an outcome of this program is both that A) advanced functionalities and manufacturability added to  "advancing specific existing pieces of OScH" being incubated in the GOSH community; and B) the possible creation of a "Hardware Carpentry" type of program. Three extended format "collaborations" are imagined. 

Propose new timeline for the GOSH Collaborative Development Program that goes to the end of 2023

Propose spending—of the $110,000: 

- Spend 75% on well-established projects and 25% on new projects 

- subdivide this so that there's a $# per project for: project leads, professional advisors, documentors, event hosting costs, ...no travel costs in 2022, but maybe in 2023. 

- Create a program manager role...halftime? 

- Determine if the Working Group IS the "a panel of community reviewers that represent a range of backgrounds, nationalities and relationships to OScH" or if the WG chooses this panel? 

Propose decision-making: will the WG make recommendations together that the Council person seated on the WG will approve, or will the WG pass recommendations to the full council to decide? 

Propose logistics: the 2022 iteration will be entirely online. The 2023 iteration may be in person. 

October: 

Study the model of—and get in touch with—Open Hardware Makers as their application process includes a track for mentors as well as for projects (OpenHardware.space) similar to what a Hardware Carpentry program might be. Also Open Hardware Leaders: https://olx-hardware.gitlab.io/ohlwebsite/ as it was mentioned in the grant. 

Issue Save The Date for a possible November "GOSH Gets Creative" session on the question, "what types of expertise are needed to bring our flagship open hardware projects to higher levels of functionality and manufacturability?"

Scope the Working Group, compare with scope that may belong to other groups:

- Set the values: GOSH community values, GOSH Code of Conduct. Further: transparent application process? if so, how to protect early IP of the global south from vultures?

Options:

Creative Common License

OSHA Certification (https://certification.oshwa.org/)

- To design the distribution of the money (strategic and equitable)? Which are the budget categories?

Options:

Well-Known Project (70%)

New Projects (30%)

- To create the application process?

- Define how to assess "the topic’s potential to meet the recommendations identified in the roadmap and its usefulness to users of OScH in addition to feasibility"

- Define demographic criteria?

- Define how projects should express their needs?

- Define how projects should express their intended outcomes?

- Define documentation and reporting requirements? 

- Design the experience "a three week online and in-person collaboration"

- Promote the program? 

- Communicate with candidates?

- Review and approve/reject candidates?

- Notify candidates? 

Create process for joining the Working Group

Populate the Working Group with people in roles

Create categories: 1. Well-Known projects that need to be advanced to the next level of functionality or reproducability; 2. seed money for new ideas that meet crtieria but have no results yet).

1. For an existing project that needs new types of expertise that do not exist on current team, and needs these advisors or other people to be paid. 

2. For new ideas, hold a workshop to teach best practices in prototyping and documentation (could form the basis of a future "Hardware Carpentry" class)

Post public "Save The Date" about when the funding opportunities will open and for how long, and what the review process will be. 

November: 

Hold GOSH creative session open to all on the question, "what types of expertise are needed to bring our flagship open hardware projects to higher levels of functionality and manufacturability?"

WG members do outreach to possible adjacent professionals with needed expertise

* this will likely have to continue throughout the two years of this program

Open application period begins

December: 

Close application period

Review applications

Send acceptance letters and confirm participation for 2022


**2022**

Q1

January: nothing happens / we prepare stuff like a lot of When2Meet polls and Zoom links

Begin Spring session

--> THINGS HAPPEN

Reporting is submitted by 1st round grantees

Q2

Begin Summer session

--> THINGS HAPPEN

Reporting is submitted by 2nd round grantees

Q3

Begin Fall session

--> THINGS HAPPEN

Reporting is submitted by 3rd round grantees

Q4

November: GOSH 2022????

Evaluate Year 1 of this program. Make changes for Year 2!

Open application period begins

Close application period

December: Review applications

Send acceptance letters and confirm participation for 2022


**2023**

Q1

January: nothing happens

Begin Spring session

--> THINGS HAPPEN

Reporting is submitted by 4th round grantees

Q2

Begin Summer session

--> THINGS HAPPEN

Reporting is submitted by 5th round grantees

Q3

Begin Fall session

--> THINGS HAPPEN

Reporting is submitted by 6th round grantees

Q4

December: Submit final report to Sloane
