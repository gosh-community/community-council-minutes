#! /usr/bin/env python3

'''
This creates a base website for GOSH council minuites.
Based off the OFEP website for OpenFlexure
'''

import os
import shutil
import re
from markdown import markdown
from jinja2 import Environment, FileSystemLoader

def _pop_starting_with(pathlist, beginning):
    '''
    Useful little function to speed up directory walking. From GitBuilding
    '''
    poplist = []
    for i, path in enumerate(pathlist):
        if path.startswith(beginning):
            poplist.append(i)
    #Pop them from the directory list in reverse order to keep indexes valid
    for i in poplist[::-1]:
        pathlist.pop(i)


class WebsiteMaker():
    '''
    This class makes a website. Oh dear, maybe I am making a static site
    generator :(
    '''
    def __init__(self):
        loader = FileSystemLoader(['_GenerateSite'])
        self.env = Environment(loader=loader, trim_blocks=True)
        self._minutes = []

    @property
    def minutes(self):
        return sorted(self._minutes)

    def copy_static(self):
        '''
        Copies in some static files
        '''
        static_dir = '_GenerateSite/static/'
        for static_file in os.listdir(static_dir):
            filepath = os.path.join(static_dir, static_file)
            outpath = os.path.join('_site', static_file)
            shutil.copyfile(filepath, outpath)

    def copy_assets(self):
        '''
        Copies in the assets (does not copy in directories, just files)
        '''
        os.makedirs(os.path.join('_site', 'assets'), exist_ok=True)
        static_dir = 'assets'
        for static_file in os.listdir(static_dir):
            filepath = os.path.join(static_dir, static_file)
            outpath = os.path.join('_site', 'assets', static_file)
            shutil.copyfile(filepath, outpath)

    def get_html(self, md, root="./"):
        '''
        Simple makdown to html converter
        '''

        # Convert GitLab flavoured markdown amth syntax into a format KaTeX will auto render
        # Note we do this rather than modify the autorender as otherwise the markdown converter will
        # catch the back-ticked tex code
        md = re.sub(r'\$`([^`]+)`\$', r'$\g<1>$', md)
        md = re.sub(r'```math((?:.|\n)*?)```', r'$$\g<1>$$', md)

        content_html = markdown(md, extensions=["markdown.extensions.tables",
                                                "markdown.extensions.attr_list",
                                                "markdown.extensions.fenced_code"])
        tmpl = self.env.get_template("template.jinja")
        html = tmpl.render(title="GOSH Community Council Meeting Minutes",
                           content=content_html,
                           root=root)

        return html

    def make_page(self, filename):
        '''
        Creates minutes
        '''
        with open(filename, 'r') as file_obj:
            md = file_obj.read()
        title = filename.split('.')[0]
        outfile = os.path.join("_site", title+'.html')
        out_dir = os.path.split(outfile)[0]
        os.makedirs(out_dir, exist_ok=True)
        root = os.path.relpath('_site', out_dir)+'/'
        html = self.get_html(md, root=root)
        with open(outfile, 'w') as file_obj:
            file_obj.write(html)
        minute_pattern = r"^[0-9][0-9][0-9][0-9]\-[0-9][0-9]\-[0-9][0-9]$"
        if re.match(minute_pattern, title) is not None:
            self._minutes.append(title)

    def make_index(self):
        """
        Make an index page
        """
        md = "# GOSH Community Council Meeting Minutes\n\n"
        md += "These are the full meeting notes as taken in the GOSH Community Council. "
        md += "They are not curated, but provided in full as written during the meeting, "
        md += "for transparency. Curated summaries will be posted to the GOSH forum.\n\n"
        md += "## Meeting index:\n\n"
        for title in self.minutes:
            md += f"* [{title}](./{title}.html)\n"
        html = self.get_html(md, root="./")
        index_page = os.path.join("_site", "index.html")
        with open(index_page, 'w') as file_obj:
            file_obj.write(html)

def main():
    '''Quick script to copy the relevant files'''

    if os.path.exists('_site'):
        shutil.rmtree('_site')
    os.mkdir('_site')

    if not os.path.exists('README.md'):
        raise RuntimeError('Are you executing this from repo root directory?')

    web_maker = WebsiteMaker()
    web_maker.copy_static()
    web_maker.copy_assets()

    for root, dirs, files in os.walk('.'):
        # From GitBuilding, This just ignores everything in hidden
        # Underscored directories
        _pop_starting_with(dirs, '.')
        _pop_starting_with(files, '.')
        _pop_starting_with(dirs, '_')
        _pop_starting_with(files, '_')

        for fname in files:
            if fname != 'README.md':
                if fname.endswith('.md'):
                    fullpath = os.path.normpath(os.path.join(root, fname))
                    web_maker.make_page(fullpath)
    web_maker.make_index()

if __name__ == "__main__":
    main()
