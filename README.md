# Minutes for the GOSH community council meetings

This repository contains the full meeting notes as taken in the GOSH Community Council. They are not curated, but provided in full as written during the meeting, for transparency. Curated summaries will be posted to the GOSH forum.

They are best viewed in their [HTML form](https://gosh-community.gitlab.io/community-council-minutes/)