Scoping and role selection for point person to set up new working group for funding distribution

Term Expires: in 3 weeks or Next Milestone (whichever comes first, pending definition of milestone)

-----

Understand context: What givens are at play regarding the distribution of funding that GOSH Inc is holding?  What external triggers are causing us to take up this issue at this time? 

Explore needs: what do you think this is really about for everyone involved? 

Synthesize the issue and underlying needs: what is the driving statement? 

* (NOTE: if the above is too complex and is essentially the work that need to be done, this can be passed off to the Working Group that is being formed to synthesize and send back)

-----

Understand the role: what is the role description? 

Explore qualifications: what qualities would you like to see in this role? 

Synthesize: consent to list of qualifications

-----

Nominations: round 1

HPY - Pierre (PP)

AQ:  Anyone

PP -  LO

LO: eb

MF pierre

EB pierre

JS - pen

Nominations: round 2 (how wonderful to be in a space of high integrity where it's safe to change your mind)

HPY - Pierre (PP)

AQ : Pierre

PP - PEN

LO -  PP

MF - Pierre

EB - PP

JS - Pierre

PROPOSAL: 

Pierre will serve as the point person for the funding distribution working group establishment for the next three weeks or until they decide they are ready to do a revote for this role.

OBJECTIONS ROUND: 

HPY -none

AQ -none

PP -none

LO -none

MF  -none

EB -none

JS - No-objections!
