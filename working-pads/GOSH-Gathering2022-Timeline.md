Possible Timeline for the GOSH Global Gathering 2022
(All dates are placeholders)

1 September 2021

* Hunt for working group volunteers

1 October 2021

* Timeline presented to GOSH Inc for extending Funding

31 October 2021

* Core Working Group Established


### PLANNING

1 November 2021

* Working group hunts for location


15 December 

* Location and Host Group Confirmed


15 -30 January 2022

* Organizer Meetup for Planning


1 - 28 Feb 2022 

* Organize all information for applications to open
* Location
* Dates
* Budget
* Program
* Funding

### APPLICATION PERIOD

1 March - 1 April

* Applications open

1 April - 15 April

* Buffer zone


15 April - 1 May

* Applications Organized for Review

1 May - 1 June 2022

* Applications reviewed

June 10 2022

* Notification of Acceptance to applicants

### PRE EVENT ORGANIZATION

15 June 2022

* Housing  Blocks Secured


1 July 2022

* All attendees confirm intent to attend GOSH 2022
* Those eligible for travel support confirm their acceptance

2 June 2022

* Call for Event Volunteers
* Documentation
* Activities
* Fun things

15 June- 15 Sep 2022

* Visa Booking period

15 Sep 2022 

* All Visas in Hand
* If an attendee is being supported, this is the cutoff date for having a visa

1 Oct 2022 

*  All travel booked

15 Oct 2022 

* All accomodations booked if we are booking for them
* Everyone else is on their own

29 Oct 2022 

* All organizers arrive and prepare

### EVENT

>>>>1-4November 2022 -  GOSH Global Gathering 2022<<<<

### POST EVENT

10 November 2022

* Event Evaluations Turned in

1 December 2022

* Reimbursement Requests are Due

6 December 2022

* All outstanding fees and services paid for

15 January 2023 

* All Reimbursements Complete