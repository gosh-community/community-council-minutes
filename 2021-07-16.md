# Notes from conversation Andrew/Liz 2021-07-16
## Philosophical beginnings of holding power together OR "The First Step's A Doozy"

- membership in GOSH is constructed through a code of conduct that establishes a basis for co-presence + interaction
- eligibility to vote is constructed through defined modes and levels of participation in GOSH
- eligibility to run for office is constructed through defined modes and levels of participation in GOSH
- electoral procedure 
- elected representatives construct a basis for sharing power among each other, going to assume on traditions of human rights and individual autonomy that each representative 


## Drafting a statement for email:

By adding your +1 via Replying All to this email, you approve the following statement:
> During the start-up phase, GOSH Council makes decisions by 100% agreement of the representatives

Slight modification suggested by JS
> Until the GOSH community council has agreed a process for decision making, all decisions will be made by 100% agreement of the representatives


When we next meet in person, we will read this statement out loud, and type it into a text file that we screenshare over video and take a screenshot with everyone thumbs-upping this statement as a formal recording of this decision."