
# GOSH Community Council Meeting 2021-07-15

Present: (asynchronously type your name below)

- lizbarry (initials: "EB")
- Pen-Yuan Hsing (initials: "HPY")
- Andrew Quitmeyer (AQ )
- Bri Johns (Bri)
- Julian Stirling (JS)
- Laura (initials: "LO")
- PIerre Padilla (PP)


## Check-in: (follow the order of the names above for a check-in / warm-up)

ANDREW: Computer tragedies OR nominate ourselves as the animal we are


## Agenda ideas: 

- Our processes
- Ideas for speaking order
- Modes of decision-making
- Chairing / facilitating rotating
- Modes of Communication
- How to handle quorum / how do we rotate and schedule
-  What to suggest as agenda items for GOSH nonprofit meeting
-  What agenda items to take next meeting? 
- EB: suggest Working Groups --- "not so structured" or "structured"
- LO: how will we communicate to full GOSH community, how will we listen to full GOSH community
- Communicating out clearly 
- EB: suggest another doc to list the proposals we've consented to, and the term limit of each decision before they expire and have to be renewed or changed. This "Policy Index" can be a reference for the whole community
- HPY: I like the idea of a term limit for decisions. Probably something for later, but I'd be happy to discuss how to implement this (e.g. automated reminders when a decision expires).


## Agenda (CONSENSUS)

Ideas for speaking order: 

EB: suggest mostly using full rounds instead of stack (i.e. speak in the order in which people raised their hands) so everyone knows their turn will come, and they can simply pass if they have nothing to add. Rounds dedicated to "context" "reactions" "ideas" etc that are re-usable for any topic, and are not dependent on a "magic insightful question" to draw out our collective intelligence

AQ: thinking of the philosophical possibillity. The values EB added of "hearing all voices" and "a group being greater than the sum of its parts" may not in fact be all agreed to, they were just stated. 

EB: dealing with the need to hear all voices; call a full round in the reverse order as before 

AQ: agreeing with all that was said, we don't have all the voices though and what does that mean even for this schedule; what if some of us disagree with values said

PP: feel that he is learning too, about how to make decisions, sometimes it is not easy, in engineering deal with lots of gender issues so special care is needed in dealing with consensus issues.

EB: (reflecting) first time really thinking about how consensus was - comment on how speaking order should be?

PP: no specific comment on this, what to do when one of us has a comment and/or wants to pass

EB: say "check" or whatever word they want when they are done?

AQ: who?

EB: only "check" yourself. "end transmission"

AQ: it's like "over" on the radio

JS: if we are in agreement no need for repeating yourself; going around in a loop makes sure everyone has opprotunity to speak. downside is slowing down discourse.. definitely topics where we will get into the weeds. Two knowledgable people, way for them to say things before those listening can jump in

Bri: pass

LO: starting with a round is OK, all voices have a chance to be heard. Not experienced in what is the best way. Participated in modes in which people name the next speaker and that is how it was formed. Also a moderator who sometimes named the turns of the poeple (keeping stack). Round is OK, but unsure aboutmoderator system as well. First point: agrees with the round.

HPY: Not too much to add. Just shares philosphical interst- can going to rounds scale up to bigger groups? of say 20? how do you scale up different ways of giving everyone a voice. even on thie process itself. 

EB: can add two things then make a proposal. 1) to JS comment, full rounds work best when each topic has the discussion strcutred so that there is a round for context first (those knowledgable go first), then if it needs to be explored/decided on there is a clear order of rounds. less like adding a weird structure on a natural conversation, and more like a machine processing info such as emotions, perspectives and needs 2) to HPY full rounds are good for central coordinating committee, small groups of elected leaders, nesting smaller circles is how to scale this
Propose: for today, we do the remainder of our agenda items in full rounds? thumbs up or raised hands

JS: just raised hand to see how it works

EB: facilitator asks for your objection when you raise hand, peoples questions and reactions should have already been heard before seeking a round of approval. still calling this consensus because it is the only place to start -once someone raises objection it needs to be heard and understood by facilitator, can workshop that objection, or if it is based on personal preference that doesn't stop decision. if group doesnt like facilitator decision, new facilitator next time.... EB can you add more here?

JS: say what decision would be, then full round discussion on decision itself, then vote
 - what if something new is added do we go through a round again
 - I think i understand though now

Modes of decision-making:

**CONTEXT ROUND:**

EB: suggest "Consent OR Objection.... if outside range of tolerance and would cause damage to mission" rather than varying %s of vote

EB: go first for context, started meeting discussing default, which is as indivdiuals we show up here in our power, any one ind. in consesnsu can stop a decision for a group, in consensus not much talked about but clarity needed on what is being discussed, not much specification on quality of proposal. groups try to protect everyones rights by saying "consensus" but haven't been great containing information and there is a lack of clarity. Nerdy on how we keep information here, global best facilitation practice, set up something "facts feelings ideas or decisions" when we process info together we need a space to articulate what we know and share that so we know together. Note what our question spaces are and clearly define what we need to know to make a decision. 

Also have to know how we are feeling, perspectives or attitudes we are bringing, this is all valid info.

what are our needs? this is valid too 
when you plainly ask these things, how are we reacting to all this, and elements of how to approach this. taking those as organized rounds lets us write a clear proposal, or even needs statement, so that when you ask peopple if you agree with this it means something that is info processing moving from consensus to consent: means we understand our domain very well; domain is caring for GOSH community! don't want to harm vision in manifesto and roadmaps - how do we combine efforts in a nonoppressive way. keeping domain in mind helps us assess when a decision may cause damage. Understanding what objection people have made allows for consent based decision making, consent to a propsal in which we full understand knowns unkowns, needs, etc.

always a time limit..are we approving things for a month or a year - clearly say it is not perfect but I CONSENT.

Not someone overriding with an objection, consent further specifies the groups current default, which is everyone says yes or no. 

Open up know to full round for those to add your context on decision making then next round is questions. anyone want to go first and call round from there

PP: in my case, has some experience from participating in courses related to this in learning, for these courses we need to share methodologies, need to protoype, how do you go from idea to physical object. in this case, we are promoting critical thinking, thinks about peer review process, trying to learn/understand context problem THEN present proposal. Share their ideas, then try to discuss, which is positive for aim of project. don't try to invalidate ideas, are not wrong ideas. but they have to decide and take the best of the ideas for a new proposal, need disagreement - to go to next step, to start prottyping, they need to decide and define requirements related to first ideas. 

LO: reflecting about this in her experiences she has had with decision making, don't have much experience in consensus based decision making, mostly in groups shes been in there was proposal with little discussion, usually things got stuck in yes or no, gosh presents new way doing this, likes the consensus based model that has been mentioned.

JS: elected official for a parish with less than 100 people, small level government, used to somebody proposes (for x amount of time) and then a second person seconds it, half-questions problem of people disliking both options when voting, but if someone raises a point and someone seconds it at least it can come up to vote. It does not have to go through a facilitator. I may have an issue with this in the future. This is about how we form the agenda.

HPY: Concern over tyranny of minority, one person could filibuster decision, that might be ok, some of the times, someone has good-faithed objection that is important, and it may be good to not make that decision because of this, but what do we do if it is something personal to someone - how do we address this? but specific concern is who gets to draw that line as to how much the objection matters to the group

AQ: seems to him that consensus model relies a lot on trust and respect of the peers you are working with. problems with any of these forms, just jumping to extremes of how do we kick out problem person- other context anything boiled down to "do-ocracy", things got done because someone did them

HPY: seconds what AQ said, someone often just takes initative to do something and everyone else just lives with it. 

**QUESTIONS ROUND**

EB: what questions do you have about what was shared in context round? Also have a question that you think if it was answered would help make a good decision.

HPY: round again?

EB: love to ask: for idea that someone may be a bad actor, ask if you can imagine that happening in our context where GOSH has elected people, step of boundary management before getting to the consent type space.

PP: pass

LO: don't know if she understands the case? meaning if she imagines a scene that andrew mentioned?

EB: no that was just posed to the group, do you have question about context?

LO: what would be a different/opposite model to consensus? majority model? meaning there is half+1? is that other alternative?

- Answer (EB): Autocracy

JS: the way a lot of discussion has been framed is starting with consensus and move towards something better, but don't get what we are moving towards. Not sure where we are planning to move from this. Second HPY worry about what to do with major vs minor disagremeents and deeming who's opinons were valid versus being outvoted (being outvoted may make you feel less angry then being told your opinion isnt valid)

JS: more response here: that voting is clear how to move to block or unblock. If people who are facilitator have a way they already know, then this will have influence over the group. If I had facilitated, we'd be in a much different place now. I'm feeling like i can't dig into an important question if it's so divided in time. (also see below under REACTIONS)

- Answer AQ: trying to crush chicken and egg problem, trying to at least make a unanimous group vote as the goal for then figuring out how we make decisions later on

HPY:

1. enjoyed EB intro in beginning, lots to digest, still not clear on precise distinction between consensus and consent. Feeling he got is different levels of how happy you are to live with the decision. not understanding core decision.
2. having to keep track of rounds makes him forget what to ask and who to ask that question to? is there something that happens, something we can deal with? 

- Answer (EB): in consensus when someone doesn't agree, there can be a sloppy process, can discuss until everyone is tired and thinks theyare on the same page. for consent it is giving us tools to understand what is the domain to make a decision about (for us its tough because our whole domain isn't clear) say we are making a decision on finance, and worked with nonprofit to decide community is going to do peer funding, and council designateds working group on which projects are selected for peer funding, we would define that domain, inside of that we would hear from council people (but how do we include broader community) but for us 7, how do we understand/bring together best insights from us 7, but also understand when deep inequities occurs someone needs to throw in an objection because it violates manifesto - we recognize that as an objection worth revisiting proposal. In short: consent preserves as much of the individual right that we have walked in the door with, while fine tuning it that by domain and by role by decision on a term so that we can get to more decisions that we can get to faster and be less autocratic.
- Answer (PP): Consensus more group level ; consent a more personal level. now we know definitions a little more and then with context shared by EB we know how to implement this. Keeping in line with manifesto/roadmap.Keeping in line with various communities? PP knows they are concerned with communities in Peru and South America. Also aware of bias as an academic as well. This will certainly be a process. Be aware that what we decide will have some positive and negative impact, and as a group we don't have whole perspective. Also agrees with EB most recent answer. 
- Answer (LB): moving from consent to consensus [NB: from consensus to consent], way of making decisions, a situated way. so all perspectives can be heard. Good way to go! 

AQ: seconds pen on consent vs consensus

**REACTIONS ROUND**

JS: big worry is that consent poorly defined, voting it is very definite how you stop a stalemate, at some point you need a method to make a decision, do worry with process we are going to gives too much power to facilitator. If he'd facilitated it would've been completely different meeting and we would be in a completely different place by now. No one disagrees with how EB runs meeting therefore we all keep doing it. Lost the nuance in people's questions, can't dig into a question if it is so divided in time. Slightly negative but important.

HPY: relates to last part JS was talking about, having trouble keeping track of things, lots of questions, lots of answers, hard to remember it all. Some questions are more suitable to a back and forth thing, sometimes can make the conversation a little bit easier. 

One thing they find reassuring, is that although he is not taking responsibility for it, when people come up with codes of conduct that is setting boundaries, and those excercises come out positively, once boundaries are set, things run smoothly, good example of successfully drawing and implementing lines

**PROPOSAL**

**CONSENT ROUND**

**CLOSING**

- need to end with action points
- need to decide what we will talk about in next meeting
- can we test out different facilitation models
- Monday 7pm

Chairing / facilitating rotating

Modes of Communication


How to handle quorum

EB: suggest a 24-48 hour window for those who weren't present to catch up on notes and read the proposal and lodge consent/objections before a proposal is officially consented to


**Notes:**

- icebreakers
Jumping into the stuff!
- liz wrote down suggested agenda ideas
- what do we want to suggest as agenda items for GOSH nonprofit meeting?
- what we think about this agenda

Start off first with HPY

- HPY: likes items, partciularly interested in modes for decision making, another item they are thinking about, technical item, which is different modes of communication going forward (been using email) but a few of us signed on to that matrix room, using jitsi meetings, GitLab Issues, etc. what do we need to discuss in terms of tools going forward.
- LO: Also interested in tackling issue of decicion making process, at what point what will be feedback channel for whole community? How do we deal with this? Will we inform the community issue we are discussing? Open discussion? How will they be participating? 
- EB: Discussion for this meeting or next?
- LO: for next one for sure
- HPY: make note that not all of us can make this meeting time, timing stuff needs to be agenda item worth discussing
- JS: quorum...
- EB: quorum/how often do we ask again what meeting time, how do we do scheduling
- Bri: ok with note taking, heads up nonprofit putting toggether agenda for that meeting
- JS: agenda great, chairing together speaking order, excited for this, open to new ideas for running meetings
- PP: still trying to decide on things that have been discussed in emails, still trying to decide on whether things like etherpad are ok, just some channels for asking as well as for the community. to define decision making process as well
- AQ: main thing is figuring out how we are going to make decsions, feeling precarious until we agree to that, then amongst us, how are we making the decision on this? excited to see how these rules get made
- PP: like speculative design
- EB: want to talk about decision making, agenda request in here?
- AQ: just new to formalized modes of interacting (I.e taking minutes) 
- EB: future meeting/social hour would be to share those experiences as to how we have held power with other people, could be traumas in here
- AQ: whole field you didn't know existed, people having whole books on how to make decisions
- JS: meta-question on how do we decide how decide before we decide decision making; chicken and an egg on decision making
- EB: assumption of consensus, reaching a point when we all agree? point of objection - someone saying personal preference vs something that hurts the overall mission moving from consensus to consent; suggest what mode we are in is consensus (that we all have to agree to modes of decision making)
- AQ: 100% vote in order for it to pass
- EB: yes, have to have heard 100% of the voices perhaps suggested agenda, speaking order, then decision making, ... adjusting the preliminary agenda, call for thumbs up if you agree to agenda, if not then fix it up so everything below row 33 matches that
- HPY: agrees on consensus
- LO: define what you mean by consensus as starting point
- EB: good question, tyranny of the minority - opposite of GOSH, consensus is not as fully definied as it would need to be, no requirement that proposal be clearly written such that it is documented and everyone has a full agreement on what they are agreeing on - awareness that there is not an attached process and being really clear that 100% has been reached, and quality of what has gone into a proposal.
  - everyone says they are in agreement (our very first decision!)
  - need adequate documentation for a good consensus to be made. need to be CLEAR on agreeing on something.
  - we have our processes here, can people review 19-30 and thumbs up if good
  - consensus moment, we all agree, we have already made a decision together and that is GREAT!
- EB: inserting core values of GOSH
  - need to hear all voices, strive to be greater than the sum of our parts
  - reaching a higher common denominator
- EB: dealing with the need to hear all voices; call a full round in the reverse order as before 
- AQ: agreeing with all that was said, we don't have all the voices though and what does that mean even for this schedule; what if some of us disagree with values said
- PP: feel that he is learning too, about how to make decisions, sometimes it is not easy, in engineering 

 
- AQ- comment thought-Something i might enjoy at some point might be a description of exactly what values and models we are implicitly incorporating into what we are doing going forward. For instance, I liked the statements where EB put in that "we need to hear all voices", (which I agree is a good idea, but not all forms of governance would technically follow that? And like even with that statement, like what about the voices of the very young? or the very old? or incapacitated? or non-human?) these are all philosophical things that can go forever in a discussion, but it would be kinda cool to be straightforward with "these are the basic models we seem to be following"
 
## We are at time, lets do a checkout time: 

- EB: JS facilitating next time?
- JS: noting that we come to a place based on how things are facilitated
- EB: temperature check, don't have a decision on how to make a decision, what do you need to say before leaving, and any feedback for the facilitator
- HPY:
    1. actually really enjoyed this experience, it is novel, and really appreciate EB facilitation
    2. relationship between what EB talking about and JS said, and realizing how seductive voting is as it is so clear cut and easy and efficient
    3. wonder if it would be a good experience for us to go through different kinds of facilitation
    4. end with specific action points that need to be taken, then have a general idea of what will be discussed next. 
- LO: felt very comfortable, now a bit anxious about how we will manage decision making ; voting process, make some trials of different ways of making decisions, maybe we can try/experience a couple of models and leave feedback on each one. but also know that these different expirements shouldnt take away from getting things done/and getting through agenda. Time management is an issue, as well as managing who keeps track of time, and facilitating in next meeting. 
- JS: agrees with LO, but wants to also slow down and better understand a process before picking one. but if we spend entire time trying to decide on this we lose time. Agrees with rotating. big difference between facilitating and chairing. maybe one is better than other... big difference if facilitator is making a decision, hard to tell what decisions are facilitation and what decisions are points. either need clear delinieation or an outside facilitator
- AQ: would personally like is if we could within the next week potentially all make a decision on if we agree that all 7 of us agreeing to something will be the first way we make a decisions on something, need to ask maria - main point of anxiety. goal that if we can either do that or not do that, then we can move forward on proposing whether we need 3/4 vote or consensus vote or quorom or whatever
- PP: shows we care about making a decision, shows we care about the process. that is what we saw in this session. maybe we don't need to reinvent the wheel, learned a lot in the session, worried about not having all the members in this session when discussing how to make a decision. Was thinking about (as a possible result) and based on HPY comments on CoC would be good to have on this topic. 
- EB: thanks to group, and feeling incomplete as we didn't form a proposal, yet shy to ask for more time. Appreciated check out rounds, to end with action points, we know what is up against us to decide on, don't understand AQ request, but I guess we will..may need professional development time that is not formal, doesn't agree with voting

Staying to discuss crabfit poll to send out as an email